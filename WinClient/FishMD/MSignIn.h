#pragma once
#include "MCommon.h"
#include <QDialog>
#include "ui_MSignIn.h"
#include "MHttp.h"

class MSignIn : public QDialog
{
	Q_OBJECT

public:
	MSignIn(QWidget *parent = Q_NULLPTR);
	~MSignIn();

	virtual bool nativeEvent(const QByteArray &eventType, void *message, long *result);

public:
	void onLoginCallback(int status, const MResponseData &data);
	QString getUserName() { return m_userName; }

private:
	void init();
private slots:
	void signIn();
	void signUp();
private:
	Ui::MSignIn ui;
	int m_nBorder;
	QString m_userName;
};
