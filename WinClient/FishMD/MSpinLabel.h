#pragma once

#include <QLabel>

class MSpinLabel : public QLabel
{
	Q_OBJECT

public:
	MSpinLabel(QWidget *parent);
	~MSpinLabel();

	virtual void timerEvent(QTimerEvent *event);
	virtual void paintEvent(QPaintEvent *event);

	void setSpinImage(const QString &strPixmap,int size);
	void setSpeed(int spped);

	void start(int delay = 0);
	void stop();

private:
	int m_nTimerId;
	int m_count;
	int m_size;
	int m_speed;
	QPixmap *m_pixmap;
	int m_delayCount;
};
