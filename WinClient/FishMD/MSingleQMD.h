#pragma once

#include <QObject>
#include <QLocalServer>
#include <QLocalSocket>
/************************************************************************/
/* 
只允许一个Fish-MD进程
*/
/************************************************************************/

class MSingleQMD : public QObject
{
	Q_OBJECT

public:
	MSingleQMD(QObject *parent = nullptr);
	~MSingleQMD();

	bool init(const QString &serverName);
	void setCurMainWnd(QWidget *curMainWnd) { m_curMainWnd = curMainWnd; }

private slots:
	void newConnection();

private:
	bool isServerRun(const QString &serverName);

private:
	QLocalServer *m_server;
	QWidget *m_curMainWnd;
};
