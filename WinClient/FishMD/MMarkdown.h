#pragma once

#include <QWidget>
#include "ui_MMarkdown.h"
#include "MDocument.h"
#include "utils/MIniFile.h"
#include <QWebEngineView>
#include <QSystemTrayIcon>
#include <QPushButton>
#include <QMenu>
#include <QLabel>
#include "MSpinLabel.h"

#define TYPE_OF_SAVE 1
#define TYPE_OF_SAVE_AND_EXIT 2
#define TYPE_OF_SAVE_AND_NEW 3

class MSyncData;
class MDirTree;
class MWebEngineView;

class MMarkdown : public QWidget
{
	Q_OBJECT

public:
	MMarkdown(QWidget *parent = Q_NULLPTR);
	~MMarkdown();

	int code() const { return m_exitCode; }
private slots:
	void onActionAbout();
	void onSignout();

private slots:
	void onShowMainWnd();
	void onCloseMainWnd();
	void onSystemTrayIconEvent(QSystemTrayIcon::ActivationReason reason);


	void onAfterGetDataFromServer();
	void onAfterSetDataToServer();

	void onStartSetDataToServer();
	void onAfterSave();
	void onNetworkError(bool b);

private slots:
	void onMenuButtonClick();
	void onMinButtonClick();
	void onMaxButtonClick();
	void onMax2ButtonClick();
	void onCloseButtonClick();


protected:
	virtual bool nativeEvent(const QByteArray &eventType, void *message, long *result);
	virtual void changeEvent(QEvent *event);
	virtual void closeEvent(QCloseEvent *event);
	virtual void paintEvent(QPaintEvent *event);
	
private:
	void loadInitData();
	void createTrayIcon();

	

	void initLayout();

private:
	Ui::MMarkdown ui;
	MDocument m_doc;
	MWebEngineView *m_webview;
	MSyncData *m_syncData;
	MDirTree *m_dirTree;
	bool m_closeFlag;
	int m_nBorder;

	QPushButton *menuButton;
	QPushButton *minButton;
	QPushButton *maxButton;
	QPushButton *max2Button;
	QPushButton *closeButton;

	
	QMenu *m_optionMenu;

	int m_exitCode;


	MSpinLabel *m_spinImage;
	QLabel *m_networkMsgLabel;
};
