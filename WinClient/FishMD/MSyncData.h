#pragma once
#include "MCommon.h"
#include <QObject>
#include <vector>
#include "MHttp.h"

class MSyncData : public QObject
{
	Q_OBJECT

public:
	MSyncData(QObject *parent=nullptr);
	~MSyncData();

public:
	void getDataFromServer();
	bool setDataToServer();

private:
	void getDataCallback(int status, const MResponseData &data);
	void setDataCallback(int status, const MResponseData &data);

	int __getChangeDocsToJson(QJsonArray &docs);
	bool __getChangeDirToJson(QJsonObject &dir);

	void timerEvent(QTimerEvent *event);

signals:
	void afterGetDataFromServer();
	void afterSetDataToServer();
	void startSetDataToServer();
	void networkError(bool b);

private:
	QString m_name;
	int m_nTimerId;
	int m_delaySyncCount;
};
