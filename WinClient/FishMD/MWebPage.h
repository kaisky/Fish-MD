#pragma once

#include <QWebEnginePage>

class MWebPage : public QWebEnginePage
{
	Q_OBJECT

public:
	MWebPage(QObject *parent = nullptr);
	~MWebPage();
};
