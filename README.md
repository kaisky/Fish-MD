# Fish-MD
Fish-MD是一款非常轻量级的Markdown云笔记工具，可以支持云端的数据同步功能。

和常规的markdown编辑工具相比，增加了图片的粘贴功能，可以将剪贴板中的图片，通过ctrl+v直接将图片添加到笔记中。

**编译后的应用程序，不需要安装，解压即可试用**
>编译环境VC15 x64
https://pan.baidu.com/s/1kVR76WR#list/path=%2FFish-MD

**试用注册路径**
>http://mt.wtulip.com/qmd/sign-up.html

**口令**
>*No cross, no crown.*

## 客户端
客户端主要采用QT进行UI开发，由于习惯性的使用了boost，后期可以改为c++11的库

windows客户端依赖的库：
- boost 1.6
- sqlite 3
- qt 5.9

![image](http://mt.wtulip.com/qmd/api/image?imageId=62156f47e7ba45e52fac71dbb82bd63f)

![image](http://mt.wtulip.com/qmd/api/image?imageId=e873d2928704f4d7a94ade114af1e2ea)

## 服务器端
服务端采用laravel框架和dingo，以restful的方式导出api接口

服务器端依赖的库：
- php >= 5.5.9（建议php7）
- laravel 5.2
- dingo 1.0


## 联系方式
欢迎来骚扰
crazycooler@qq.com

## license
GPL v2

